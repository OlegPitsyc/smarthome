package com.smarthome.model;

public class Room {

    private int id;

    private int square;

    public enum Type {BATHROOM, BEDROOM, KITCHEN, LIVINGROOM, WARDROBE}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", square=" + square +
                '}';
    }
}
